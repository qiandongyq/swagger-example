const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const swaggerJSDoc = require("swagger-jsdoc");

// Api doc
const swaggerUi = require("swagger-ui-express");

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors("*"));

// Swagger definition
const swaggerDefinition = {
  info: {
    title: "Swagger example", // Title of the documentation
    version: "1.0.0", // Version of the app
    description: "This is the REST API example use swagger" // short description of the app
  },
  host: "localhost:8080", // the host or url of the app
  basePath: "/api", // the basepath of your endpoint
  schemes: ["http"]
};

// options for the swagger docs
const options = {
  // import swaggerDefinitions
  swaggerDefinition,
  // path to the API docs
  apis: ["./docs/**/*.yaml"]
  // apis: ["./index.js"]
};

const swaggerSpec = swaggerJSDoc(options);

app.use("/api/doc", swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// Server PORT user 8000 or server enviroment defined PORT (cloud)
const PORT = 8080 || process.env.PORT;

app.listen(PORT, () => console.log(`Server is running on ${PORT}`));

const posts = [
  {
    id: 1,
    title: "swagger",
    content: "I like swagger"
  },
  {
    id: 2,
    title: "example",
    content: "This is an example"
  }
];

/**
 * @swagger
 *
 * /posts:
 *   get:
 *     summary: List of all posts
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: success
 *       400:
 *         description: bad request
 */
app.get("/api/posts", (req, res) => {
  return res.send(posts);
});

// faild call
// app.get("/api/posts", (req, res) => {
//   res.status(400).send();
// });

// app.get("/api/posts/:id", (req, res) => {
//   const { id } = req.params;
//   const foundPost = posts.filter(p => p.id === Number(id));
//   if (foundPost.length === 0) {
//     return res.status(404).send("Post not found");
//   }
//   return res.send(posts.filter(p => p.id === Number(id)));
// });
